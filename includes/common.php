<?php

// test

$vendor = plugin_dir_path( dirname( __FILE__ ) ).'vendor';

/**
 * require: IworksEvents Class
 */
if ( !class_exists( 'Iworks_Facebook_Event' ) ) {
    require_once $vendor.'/iworks/facebook/event.php';
}
/**
 * configuration
 */
require_once dirname( dirname( __FILE__ )).'/etc/options.php';

/**
 * require: Iworks_Options Class
 */
if ( !class_exists( 'Iworks_Options' ) ) {
    require_once $vendor.'/iworks/options.php';
}

/**
 * i18n
 */
load_plugin_textdomain( 'iworks_facebook_event', false, dirname( dirname( plugin_basename( __FILE__) ) ).'/languages' );

/**
 * load options
 */
$iworks_facebook_event_options = new Iworks_Options();
$iworks_facebook_event_options->set_option_function_name( 'iworks_facebook_event_options' );
$iworks_facebook_event_options->set_option_prefix( IWORKS_FACEBOOK_EVENT_PREFIX );

Function iworks_facebook_event_options_init()
{
    global $iworks_facebook_event_options;
    $iworks_facebook_event_options->options_init();
}

function iworks_facebook_event_activate()
{
    $iworks_facebook_event_options = new Iworks_Options();
    $iworks_facebook_event_options->set_option_function_name( 'iworks_facebook_event_options' );
    $iworks_facebook_event_options->set_option_prefix( IWORKS_FACEBOOK_EVENT_PREFIX );
    $iworks_facebook_event_options->activate();
    flush_rewrite_rules();
}

function iworks_facebook_event_deactivate()
{
    global $iworks_facebook_event_options;
    $iworks_facebook_event_options->deactivate();
    flush_rewrite_rules();
}

