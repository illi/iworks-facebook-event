<?php

/*

Copyright 2014 Marcin Pietrzak (marcin@iworks.pl)

this program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

 */

if ( !defined( 'WPINC' ) ) {
    die;
}

if ( class_exists( 'Iworks_Facebook_Event' ) ) {
    return;
}

class Iworks_Facebook_Event
{
    private $base;
    private $capability;
    private $dev;
    private $dir;
    private $noncename;
    private $options;
    private $version;

    public function __construct()
    {
        /**
         * static settings
         */
        $this->base = dirname( dirname( dirname( __FILE__ ) ) );
        $this->dir = basename( dirname( $this->base ) );

        $this->capability = apply_filters( 'iworks_facebook_event_capability', 'edit_posts' );
        $this->dev = ( defined( 'IWORKS_DEV_MODE' ) && IWORKS_DEV_MODE )? '.dev':'';
        $this->noncename = __CLASS__;
        $this->version = '0.0.1';

        /**
         * actions & filters
         */
        add_action( 'admin_menu', array( $this, 'admin_menu' ) );
        add_action( 'admin_init', 'iworks_facebook_event_options_init' );
        add_action( 'widgets_init', array( $this, 'widgets_init' ));

        /**
         * global option object
         */
        global $iworks_facebook_event_options;
        $this->options = $iworks_facebook_event_options;
    }

    public function get_version($file = null)
    {
        if ( defined( 'IWORKS_DEV_MODE' ) && IWORKS_DEV_MODE ) {
            if ( null != $file ) {
                $file = dirname( $this->base ). $file;
                if ( is_file( $file ) ) {
                    return md5_file( $file );
                }
            }
            return rand( 0, PHP_INT_MAX );
        }
        return $this->version;
    }


    public function widgets_init()
    {
        require_once sprintf('%s/event/widget.php', dirname(__FILE__) );
        register_widget( 'Iworks_Facebook_Event_Widget' );
    }

    public function update()
    {
        $version = $this->get_option( 'version', 0 );
        if ( 0 < version_compare( $this->version, $version ) ) {
            $this->options->update_option( 'version', $this->version );
        }
    }

    /**
     * Add admin menu
     */
    public function admin_menu()
    {
        add_options_page( __( 'Facebook Event', 'iworks_facebook_event' ), __( 'Facebook Event', 'iworks_facebook_event' ), $this->capability, $this->dir.'/admin/index.php' );
    }

    private function get_option($option_name)
    {
        return $this->options->get_option($option_name);
    }

}
