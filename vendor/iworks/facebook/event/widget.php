<?php

/*

Copyright 2014 Marcin Pietrzak (marcin@iworks.pl)

this program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

 */

if ( !defined( 'WPINC' ) ) {
    die;
}

if ( class_exists( 'Iworks_Facebook_Event_Widget' ) ) {
    return;
}

class iworks_facebook_event_widget extends WP_Widget
{
    private $options;
    private $iworks_widget_options;
    private $goal = 100;
    /**
     * Sets up the widgets name etc
     */
    public function __construct()
    {
        // widget actual processes
        parent::__construct(
            __CLASS__,
            __('Facebook Event', 'iworks_facebook_event'),
            array(
                'description' => __( 'Add a Facebook Event data.', 'iworks_facebook_event' ),
            )
        );

        /**
         * settings
         */
        $this->iworks_widget_options = array(
            /*
            'title' => array(
                'label' => __( 'Event Title', 'iworks_facebook_event' ),
                'type' => 'text',
            ),
             */
            'id' => array(
                'label' => __( 'Event ID', 'iworks_facebook_event' ),
                'type' => 'text',
            ),
            'show_event_link' => array(
                'label' => __( 'Show event link', 'iworks_facebook_event' ),
                'type' => 'checkbox',
                'default' => 1,
            ),
        );

        /**
         * global option object
         */
        global $iworks_facebook_event_options;
        $this->options = $iworks_facebook_event_options;
    }

    /**
     * Outputs the content of the widget
     *
     * @param array $args
     * @param array $instance
     */
    public function widget($args, $instance)
    {
        $event_data = $this->get_event_data($instance['id'] );
        if (empty( $event_data ) ) {
            if ( current_user_can('edit_posts') ) {
                echo $args['before_widget'];
                echo $args['before_title'];
                _e('Event widget error', 'iworks_facebook_event' );
                echo $args['after_title'];
                _e('Please enter valid AppID, Secret (configuration), and event ID (widget).', 'iworks_facebook_event' );
                echo $args['after_widget'];
            }
            return;
        }
        $title = apply_filters( 'widget_title', $event_data['name'] );
        echo $args['before_widget'];
        if ( ! empty( $title ) ) {
            echo $args['before_title'];
            echo $title;
            echo $args['after_title'];
        }
        printf( '<p class="label">%s</p>', __( 'Attending', 'iworks_facebook_event' ) );
        printf( '<p class="attending">%s</p>', $event_data['attending'] );
        if ( $event_data['attending'] < $this->goal ) {
            printf( '<p class="label">%s</p>', __( 'Missing', 'iworks_facebook_event' ) );
            printf( '<p class="missing">%s</p>', $this->goal - $event_data['attending'] );
        }
        if ( $instance['show_event_link'] ) {
            printf(
                '<p class="link"><a href="https://www.facebook.com/events/%s/">%s</a></p>',
                $instance['id'],
                __('Show event on FaceBook', 'iworks_facebook_event')
            );
        }
        echo $args['after_widget'];
    }

    /**
     * Outputs the options form on admin
     *
     * @param array $instance The widget options
     */
    public function form($instance)
    {
        foreach ( $this->iworks_widget_options as $key => $data ) {
            $value = ( is_array($instance) && isset($instance[$key]))? $instance[ $key ]:(isset($data['default'])?$data['default']:'');
            printf ('<p class="type-%s item-%s">', $data['type'], $key );
            switch( $data['type'] ) {
            case 'checkbox':
                printf( '<label for="%s">', $this->get_field_id( $key ));
                printf(
                    '<input type="%s" id="%s" name="%s" %s /> ',
                    $data['type'],
                    $this->get_field_id( $key ),
                    $this->get_field_name( $key ),
                    $value? 'checked="checked"':''
                );
                echo $data['label'];
                echo '</label>';
                break;
            default:

?>
        <label for="<?php echo $this->get_field_id( $key ); ?>"><?php echo $data['label']; ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( $key ); ?>" name="<?php echo $this->get_field_name( $key ); ?>" type="text" value="<?php echo esc_attr( $value ); ?>" />
<?php
                break;
            }
            echo '</p>';
        }
    }

    /**
     * Processing widget options on save
     *
     * @param array $new_instance The new options
     * @param array $old_instance The previous options
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        foreach ( $this->iworks_widget_options as $key => $data ) {
            $instance[$key] = ( ! empty( $new_instance[$key] ) ) ? strip_tags( $new_instance[$key] ) : '';
        }
        return $instance;
    }

    private function get_option($option_name)
    {
        return $this->options->get_option($option_name);
    }

    private function get_event_data($id)
    {
        if ( empty( $id ) ) {
            return false;
        }
        $option_name = 'iworks_facebook_event_'.$id;
        $option_time = $option_name.'_time';
        $data = get_option($option_name);
        try {

            if ( empty( $data ) || time() - get_option($option_time, 0 ) > 14*60 ) {
                add_option( $option_name, 0, '', 'no' );
                add_option( $option_time, 0, '', 'no' );
                if ( !class_exists( 'BaseFacebook' ) ) {
                    require_once dirname( dirname( dirname( dirname( __FILE__ ) ) ) ).'/facebook/facebook.php';
                }
                $config = array(
                    'appId' => $this->options->get_option('id'),
                    'secret' => $this->options->get_option('secret'),
                );
                $facebook = new facebook($config);
                $fdata = $facebook->api($id, 'GET', array('summary' => 1));
                $fdata['attending'] = $facebook->api($id.'/attending', 'GET', array('summary' => 1));
                $data['name'] = $fdata['name'];
                $data['attending'] = intval( $fdata['attending']['summary']['count'] );
                update_option( $option_name, $data );
                update_option( $option_time, time() );
            }
            return $data;
        } catch (Exception $e) {
        }
        return false;
    }
}
