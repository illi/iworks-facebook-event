<?php

function iworks_facebook_event_options()
{
    $iworks_facebook_event_options = array();

    /**
     * main settings
     */
    $iworks_facebook_event_options = array(
        'index' => array(
            'use_tabs' => true,
            'version' => '0.0',
            'options' => array(
                array(
                    'type' => 'heading',
                    'label' => __( 'Facebook API', 'iworks_facebook_event' ),
                ),
                array(
                    'name' => 'id',
                    'type' => 'text',
                    'th' => __('App ID', 'iworks_facebook_event'),
                    'class' => 'widefat',
                ),
                array(
                    'name' => 'secret',
                    'type' => 'text',
                    'th' => __('App Secret', 'iworks_facebook_event'),
                    'class' => 'widefat',
                ),
            ),
        ),
    );

    return $iworks_facebook_event_options;
}
