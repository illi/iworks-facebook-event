<?php
include_once ABSPATH.'wp-admin/includes/meta-boxes.php';
$iworks_facebook_event->update();
?>
<div class="wrap">
    <h2><?php _e('Facebook Event Settings', 'iworks_facebook_event') ?></h2>
    <form method="post" action="options.php" id="iworks_facebook_event_admin_index">
        <div class="postbox-container" style="width:75%">
<?php

$option_name = basename( __FILE__, '.php');
$iworks_facebook_event_options->settings_fields( $option_name );
$iworks_facebook_event_options->build_options( $option_name );

?>
        </div>
        <div class="postbox-container" style="width:23%;margin-left:2%">
            <div class="metabox-holder">
                <div id="help" class="postbox">
                    <h3 class="hndle"><?php _e( 'Need Assistance?', 'iworks_facebook_event' ); ?></h3>
                    <div class="inside">
                        <p><?php _e( 'Problems? The links bellow can be very helpful to you', 'iworks_facebook_event' ); ?></p>
                        <ul>
                            <li><a href="mailto:<?php echo antispambot('marcin@iworks.pl'); ?>"><?php printf( __( 'Mail to me: %s', 'iworks_facebook_event' ), antispambot( 'marcin@iworks.pl' ) ); ?></a></li>
                        </ul>
                        <hr />
                        <p class="description"><?php _e('Created by: ', 'iworks_facebook_event' ); ?> <a href="http://iworks.pl/"><span>iWorks.pl</span></a></p>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
