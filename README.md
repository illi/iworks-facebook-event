iWorks Facebook Event plugin
=======================

Changelog
---------

##### 0.0.2 (June 24, 2014)

* Added widget control: link to facebook event.

##### 0.0.1 (June 24, 2014)

* INIT: import to repo

